from flask import Flask


def createAPP():
    app = Flask(__name__)

    # Routes de Facebook
    from routes.facebookRoutes import facebookBP
    app.register_blueprint(facebookBP, url_prefix='/facebook')

    # Routes de Twitter
    from routes.twitterRoutes import twitterBP
    app.register_blueprint(twitterBP, url_prefix='/twitter')

    # Routes de Twitter
    from routes.FootBallRoutes import footballBP
    app.register_blueprint(footballBP, url_prefix='/football')

    return app


if __name__ == '__main__':
    app = createAPP()
    app.run(port=8089, debug=True)
