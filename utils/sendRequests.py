import requests
import json
from utils.environment import Environment as Env


class SendRequests(Env):
    def __init__(self):
        self.URL_API = None
        self.params = None
        self.headers = None

    def get(self, service, endpoint, data = None):
        self.optionConfig(service)

        response = requests.get(
            f'{self.URL_API}/{endpoint}',
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            self.log(response.status_code)
            self.log(response.text)
        else:
            return response.text

    def post(self, service, endpoint, data = None):
        self.optionConfig(service)

        response = requests.post(
            f'{self.URL_API}/{endpoint}',
            params=self.params,
            headers=self.headers,
            data=json.dumps(data)
        )

        if response.status_code != 200:
            self.log(response.status_code)
            self.log(response.text)
        else:
            return response.text

    def optionConfig(self, service):
        if service.upper() == 'FACEBOOK':
            self.configFacebook()

        if service.upper() == 'FOOTBALL':
            self.configFootBall()

    def configFacebook(self):
        # Almacenamos las variables env de conexión a Facebook
        envFacebook = self.facebook()
        ACCESS_TOKEN = envFacebook['ACCESS_TOKEN']
        self.URL_API = envFacebook['URL_API']

        # Configuramos los parametros generales para el envió
        # de información hacía Facebook
        self.params = { "access_token": ACCESS_TOKEN }
        self. headers = { "Content-Type": "application/json" }

    def configFootBall(self):
        # Almacenamos las variables env de conexión a Facebook
        envFootball = self.footBall()
        self.URL_API = envFootball['URL_API']

        # Configuramos los parametros generales para el envió
        # de información hacía Football
        self. headers = {
            'x-rapidapi-host': "api-football-v1.p.rapidapi.com",
            'x-rapidapi-key': "7b77796e5fmsha66e885da59c380p16c3fbjsn38c876bd0705"
        }

    def log(self, message):  # funcion de logging para heroku
        print('LOG SEND REQUEST')
        print(json.dumps(message, indent=2))
