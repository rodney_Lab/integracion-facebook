from flask import Blueprint

# Import Facebook e instanciando la clase
from controllers.TwitterController import TwitterController
twitter = TwitterController()

from controllers.FootBallController import FootBallController
football = FootBallController()

# Registrando Blueprint
twitterBP = Blueprint('twitterBP', __name__)

@twitterBP.route('/')
def hola():
    return twitter.hola()


@twitterBP.route('/prueba')
def prueba():
    return football.sendMessage()
