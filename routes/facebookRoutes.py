from flask import Blueprint

# Import Facebook e instanciando la clase
from controllers.FacebookController import FacebookController
facebook = FacebookController()

# Registrando en Blueprint
facebookBP = Blueprint('facebookBP', __name__)

# Route para validar el webHook
@facebookBP.route('/webhook')
def validateWebhook():
    return facebook.validate()

# Route para recibir los mensajes o delivery de facebook
@facebookBP.route('/webhook', methods=['POST'])
def receivedMessage():
    return facebook.receivedMessage()

# Routes para enviar un mensaje string hacia facebook
@facebookBP.route('/send-message')
def sendMessage():
    return facebook.sendMessage()

@facebookBP.route('/send-postback-button')
def sendPostbackButton():
    return facebook.sendPostbackButton()

@facebookBP.route('/send-quick-replies')
def sendQuickReplies():
    return facebook.sendQuickReplies()
