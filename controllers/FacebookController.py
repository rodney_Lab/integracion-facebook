import json
import requests
from flask import request
from utils.sendRequests import SendRequests

from controllers.FootBallController import FootBallController


class FacebookController(SendRequests):
    def validate(self):
        # Obtener las variables de entorno para facebook.
        envFacebook = self.facebook()

        # Almacenamos los valores enviados como parametros en el request
        valueHubMode = request.args.get("hub.mode")
        valueHubChallenge = request.args.get("hub.challenge")
        valueHubToken = request.args.get("hub.verify_token")

        if valueHubMode == "subscribe" and valueHubChallenge:
            # Validar que el Token enviando por facebook sea idéntico al que
            # tenemos en nuestro servidor. Si es invalido devolvemos un status
            # code 403. Caso contrario devolvemos un status code 200 con el
            # valor hub.challenge
            if not valueHubToken == envFacebook['TOKEN']:
                return "Verification token mismatch", 403

            return valueHubChallenge, 200

    def sendMessage(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()

        recipientID = data['recipientID']
        messageText = data['messageText']
        data = {
            "recipient": {"id": recipientID},
            "message": {"text": messageText}
        }

        # Mostrar el efecto de escribiendo hacia el facebook
        #self.sendAction(recipientID, 'typing_on')

        # Enviando el mensaje hacia facebook
        return self.post('facebook', 'me/messages', data)

    
    def receivedMessage(self):
        data = request.get_json()

        # Imprimir lo que recibimos temporalmente.
        self.logFacebook(data)

        # Validar Type Message
        self.validateOptionReceived(data)

        return 'Hemos recibido la data', 200

    def sendAction(self, recipientID, senderAction):
        # senderAction solo soporte tres formas
        # mark_seen  : Para marcar como leído el ultimo mensaje
        # typing_on  : Para mostrar el efecto de escribir
        # typing_off : Para retirar el efecto de escribir
        data = {
            "recipient": {
                "id": recipientID
            },
            "sender_action": senderAction
        }

        self.post('facebook', 'me/messages', data)

    def validateOptionReceived(self, data):
        if data["object"] == "page":
            for entry in data["entry"]:
                for messagingEvent in entry["messaging"]:
                    senderID = messagingEvent["sender"]["id"]

                    self.logFacebook(messagingEvent)
                    # Alguien envía un mensaje
                    if messagingEvent.get("message"):
                        # Marcar como leído
                        #self.sendAction(senderID, 'mark_seen')

                        # message_text = messagingEvent["message"]["text"]
                        self.logFacebook('El usuario nos a mandado un mensaje')
                        
                        #Get list of countries
                        football=FootBallController()
                        lpais=football.listCountries()
                        print(lpais)
                        self.sedMess2("lista paises",lpais)

                    # Confirmacion de delivery
                    if messagingEvent.get("delivery"):
                        # Desactivar el efecto de escribiendo
                        self.sendAction(senderID, 'typing_off')

                        self.logFacebook('Confirmacion de delivery')

                    # Confirmacion de option
                    if messagingEvent.get("option"):
                        self.logFacebook('Confirmacion de Option')

                    # Evento cuando usuario hace click en botones
                    if messagingEvent.get("postback"):
                        self.logFacebook('Evento cuando usuario hace click en botones')

    def sendPostbackButton(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()

        recipientID = data['recipientID']
        data = {
            "recipient": {"id": recipientID},
            "message": {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type":"button",
                        "text": "Try the postback button!",
                        "buttons": [
                            {
                                "type": "postback",
                                "title": "Alianza Lima",
                                "payload": "alianzaLima"
                            },
                            {
                                "type": "postback",
                                "title": "Universitario de Deportes",
                                "payload": "universitario"
                            }
                        ]
                    }
                }
            }
        }

        return self.post('facebook', 'me/messages', data)

    def sendQuickReplies(self):
        # Obteniendo la data enviada en el body en formatoJSON
        data = request.get_json()

        recipientID = data['recipientID']
        data = {
            "recipient": {"id": recipientID},
            "messaging_type": "RESPONSE",
            "message": {
                "text": "Selecciona un Equipo",
                "quick_replies": [
                    {
                        "content_type": "text",
                        "title": "Alianza Lima",
                        "payload": "alianzaLima_QR",
                        "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Alianza_Lima.svg/1200px-Alianza_Lima.svg.png"
                    },{
                        "content_type": "text",
                        "title": "Universitario de Deportes",
                        "payload": "universitario_QR",
                        "image_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Alianza_Lima.svg/1200px-Alianza_Lima.svg.png"
                    }
                ]
            }
        }

        return self.post('facebook', 'me/messages', data)

    def logFacebook(self, data):
        print('Log Facebook')
        print(json.dumps(data, indent=2))

    def sedMess2(self,txt,lst):
        payload = request.json
        event = payload['entry'][0]['messaging']
        for x in event:
            #if is_user_message(x):
            #text = x['message']['text']
            sender_id = x['sender']['id']
               #respond(sender_id, text)
            #text="lista de paises"
            #self.send_message(sender_id, txt)
            self.send_btn4(sender_id,txt,lst)
            #self.post('FACEBOOK','me/messages', data = text)
        
        return "ok"
    def send_message(self,recipient_id, text):
        """Send a response to Facebook"""
        payload = {
        'message': {
            'text': text
        },
        'recipient': {
            'id': recipient_id
        },
        'notification_type': 'regular'
        }

        auth = {
            'access_token': 'EAAIejEjpVtsBAPBh1kQo9sbQqp2nlF0I0V0ewk5BbnnlVZBcVtZBf96462ZBrCVkINGax6gFk4ZCVF5UTbPJjWlZCyZCZCU8mtTFiLqVkYh1jzHZAG5Bkvmo3xMjo7LWwaiUZAqCwIHbT5BWxZBhuf3Pn6H1VNZBZCOrTrsJVj2uywiXXzk2jIZAavfw7tzanOINCjYwZD'
        }

        response = requests.post(
        'https://graph.facebook.com/v2.6/me/messages',
        params=auth,
        json=payload
        )

        return response.json()

    def send_btn4(self,recipient_id, text,btn4):
        envFacebook = self.facebook()
        payload={
            "recipient": {"id": recipient_id},
            "messaging_type": "RESPONSE",
            "message": {
                "text": "Selecciona un Equipo",
                "quick_replies": [
                    {
                        "content_type": "text",
                        "title": btn4[0]['country'],
                        "payload": "alianzaLima_QR",
                        "image_url":btn4[0]['flag']
                    },{
                        "content_type": "text",
                        "title": btn4[1]['country'],
                        "payload": "universitario_QR",
                        "image_url": btn4[1]['flag']
                    },{
                        "content_type": "text",
                        "title": btn4[2]['country'],
                        "payload": "universitario_QR",
                        "image_url": btn4[2]['flag']
                    },{
                        "content_type": "text",
                        "title": btn4[3]['country'],
                        "payload": "universitario_QR",
                        "image_url": btn4[3]['flag']
                    }
                ]
            }
        }
        auth = {
            'access_token': envFacebook['ACCESS_TOKEN']
        }

        response = requests.post(
        'https://graph.facebook.com/v2.6/me/messages',
        params=auth,
        json=payload
        ) 

        return response.json()